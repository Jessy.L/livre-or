<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Livre d'or</title>
</head>
<body>
    
    <form method="POST" action="new_message.php">
    
        <label for="pseudo">PSEUDO</label><br>
        <input type="text" name="pseudo" id="pseudo"> <br><br>

        <label for="message">MESSAGE</label><br>
        <textarea name="message" id="message" cols="100" rows="10"></textarea><br><br>
        
        <input type="submit" value="ENVOYER">
    </form>

<?php 
    require("bdd/bddconfig.php");

    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8",$bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        $recup = $objBdd -> query("SELECT * FROM `message` ORDER BY date DESC");

    } catch(Exception $prmE) {
        die("Erreur : " . $prmE->getMessage());
    }
?>

<div class="allmessage">
    <h1>Liste message</h1>
    
   <?php
    while($messageSimple = $recup->fetch()){
   ?>

    <div class="box">
    
        <h2> <?php echo $messageSimple["pseudo"]; ?> </h2>
        <p> <?php echo $messageSimple["date"]; ?> </p>
        <p>  <?php echo $messageSimple["message"]; ?> </p>
        
    </div>

        
   <?php
   }
   $recup->closeCursor()
   ?>

</div>


</body>
</html>