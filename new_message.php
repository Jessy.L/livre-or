<?php
$pseudo = $_POST["pseudo"];
$messageadd = $_POST["message"];

require 'bdd/bddconfig.php';
try {

    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsert = $objBdd->prepare("INSERT INTO `message` (pseudo, message) VALUES (:pseudo, :messageadd)");
        $PDOinsert->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
        $PDOinsert->bindParam(':messageadd', $messageadd, PDO::PARAM_STR);
        $PDOinsert->execute();

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
